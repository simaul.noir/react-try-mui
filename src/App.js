import React from 'react';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { IconButton } from '@mui/material';

import './App.css';
import { PhotoCamera } from '@mui/icons-material';

export default function App() {
  return (
    <div className='App'>
      <Button 
        variant='contained'
        color='warning'
        onClick={() => alert('Clicked')}
        size="large"
        endIcon={<DeleteIcon />}
      >Hello World</Button>
      
      <IconButton color='error'>
        <PhotoCamera />
      </IconButton>

    </div>
  )
}